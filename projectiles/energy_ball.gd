extends Node2D

class_name EnergyBall

const SPEED : int = 400

@onready var sound := $Sound
@onready var label := $Label

var ten := 3 : set = set_ten
var unit := 2 : set = set_unit
var can_disapear := true
var shot_at_time := 0

var velocity = Vector2.ZERO

func _physics_process(delta: float) -> void:
	global_position += velocity * delta

func fired():
	# we keep track of the time at which the bullet was shot
	# to be able to retroactively change the unit/ten after it was shot
	# if it was not shot to long ago
	shot_at_time = Time.get_ticks_msec();
	update_number()
	sound.play() 
	velocity =  Vector2(
		0,
		-1 * SPEED,
	)

func update_number():
	label.text = "%s" % (ten * 10 + unit)

func set_ten(new_ten) -> void:
	if new_ten == -1:
		new_ten = 9
		
	
	ten = new_ten % 10
	update_number()

func set_unit(new_unit) -> void:
	
	if new_unit == -1:
		new_unit = 9
		ten -= 1
	if new_unit == 10:
		ten += 1
		new_unit = 0
	unit = new_unit
	update_number()


func _on_screen_exited() -> void:
	if can_disapear:
		queue_free()


func _on_body_entered(body: Node2D) -> void:
	if not can_disapear:
		return
	if body.has_method("receive_damage"):
		body.receive_damage(ten*10 + unit)
	queue_free()
	pass # Replace with function body.
