extends Node

class_name ProjectileManager

@onready var switch_number_audio := $SwitchNumberAudio

func on_shoot(projectile: EnergyBall, output_position: Vector2, ten: int, unit: int) -> void:
	add_child(projectile)
	projectile.global_position = output_position
	projectile.fired()
	projectile.ten = ten
	projectile.unit = unit

func on_ten_changed(new_ten: int) -> void:
	if switch_number_audio != null:
		switch_number_audio.play()
	var current_time = Time.get_ticks_msec()
	for projectile in self.get_children():
		# we ignore projectile shot more than 1sec ago
		if projectile.shot_at_time < (current_time - 2000):
			continue
		projectile.ten = new_ten


func on_unit_changed(new_unit: int) -> void:
	if switch_number_audio != null:
		switch_number_audio.play()
	var current_time = Time.get_ticks_msec()
	for projectile in self.get_children():
		# we ignore projectile shot more than 1sec ago
		if projectile.shot_at_time < (current_time - 2000):
			continue
		projectile.unit = new_unit
