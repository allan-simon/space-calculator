extends Vosk

var player : AudioStreamPlayer;

signal number_said(ten, unit)

const words2Number := {
	"un": 1,
	"deux": 2,
	"trois": 3,
	"quatre": 4,
	"cinq": 5,
	"six": 6,
	"sept": 7,
	"huit": 8,
	"neuf": 9,
	"dix": 10,
	"onze": 11,
	"douze": 12,
	"treize": 13,
	"quatorze": 14,
	"quinze": 15,
	"seize": 16,
	"dix-sept": 17,
	"dix-huit": 18,
	"dix-neuf": 19,
	"vingt": 20,
	"vingt-et-un": 21,
	"vingt-deux": 22,
	"vingt-trois": 23,
	"vingt-quatre": 24,
	"vingt-cinq": 25,
	"vingt-six": 26,
	"vingt-sept": 27,
	"vingt-huit": 28,
	"vingt-neuf": 29,
	"trente": 30,
	"trente-et-un": 31,
	"trente-deux": 32,
	"trente-trois": 33,
	"trente-quatre": 34,
	"trente-cinq": 35,
	"trente-six": 36,
	"trente-sept": 37,
	"trente-huit": 38,
	"trente-neuf": 39,
	"quarante": 40,
	"quarante-et-un": 41,
	"quarante-deux": 42,
	"quarante-trois": 43,
	"quarante-quatre": 44,
	"quarante-cinq": 45,
	"quarante-six": 46,
	"quarante-sept": 47,
	"quarante-huit": 48,
	"quarante-neuf": 49,
	"cinquante": 50,
	"cinquante-et-un": 51,
	"cinquante-deux": 52,
	"cinquante-trois": 53,
	"cinquante-quatre": 54,
	"cinquante-cinq": 55,
	"cinquante-six": 56,
	"cinquante-sept": 57,
	"cinquante-huit": 58,
	"cinquante-neuf": 59,
	"soixante": 60,
	"soixante-et-un": 61,
	"soixante-deux": 62,
	"soixante-trois": 63,
	"soixante-quatre": 64,
	"soixante-cinq": 65,
	"soixante-six": 66,
	"soixante-sept": 67,
	"soixante-huit": 68,
	"soixante-neuf": 69,
	"soixante-dix": 70,
	"soixante-et-onze": 71,
	"soixante-douze": 72,
	"soixante-treize": 73,
	"soixante-quatorze": 74,
	"soixante-quinze": 75,
	"soixante-seize": 76,
	"soixante-dix-sept": 77,
	"soixante-dix-huit": 78,
	"soixante-dix-neuf": 79,
	"quatre-vingt": 80,
	"quatre-vingt-un": 81,
	"quatre-vingt-deux": 82,
	"quatre-vingt-trois": 83,
	"quatre-vingt-quatre": 84,
	"quatre-vingt-cinq": 85,
	"quatre-vingt-six": 86,
	"quatre-vingt-sept": 87,
	"quatre-vingt-huit": 88,
	"quatre-vingt-neuf": 89,
	"quatre-vingt-dix": 90,
	"quatre-vingt-onze": 91,
	"quatre-vingt-douze": 92,
	"quatre-vingt-treize": 93,
	"quatre-vingt-quatorze": 94,
	"quatre-vingt-quinze": 95,
	"quatre-vingt-seize": 96,
	"quatre-vingt-dix-sept": 97,
	"quatre-vingt-dix-huit": 98,
	"quatre-vingt-dix-neuf": 99,
}

var effect : AudioEffectCapture  # See AudioEffect in docs
var buffer_length_frames : int

func _ready() -> void:
	init("bin/vosk-model-small-fr-0.22", JSON.stringify(words2Number.keys()))
	AudioServer.set_bus_layout(load("res://default_bus_layout.tres"))
	var idx = AudioServer.get_bus_index("Record")
	effect = AudioServer.get_bus_effect(idx, 0)
	buffer_length_frames = effect.get_buffer_length_frames()
	
	player = AudioStreamPlayer.new()
	player.stream = AudioStreamMicrophone.new()
	player.bus = "Record";
	player.autoplay = true;
	add_child(player)
	
	pass # Replace with function body.


func _process(delta: float) -> void:

	if  !effect.can_get_buffer(buffer_length_frames / 2):
		return

	var data = effect.get_buffer(effect.get_frames_available())
	if accept_wave_form_stereo_float(data):
		print(result())
	else:
		var one_partial_result = JSON.parse_string(partial_result())["partial"]
		if one_partial_result != '':

			var number = words2Number.get(one_partial_result.replace(' ', '-'), null)
			if number != null:
				print(number)
				number_said.emit(
					number / 10,
					number % 10,
				)
	effect.clear_buffer()
