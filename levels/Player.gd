extends CharacterBody2D

class_name Player

signal shoot
signal died

const SPEED = 500

@export var Weapon : PackedScene
@onready var gun_ouput := $GunOuput
@onready var energy_ball := $EnergyBall

@onready var explosionSound := $ExplosionSound
@onready var state := $State
@onready var timer := $Timer

var dying := false

func _ready() -> void:
	dying = false
	energy_ball.can_disapear = false
	energy_ball.unit = state.unit
	energy_ball.ten = state.ten
	state.ten_changed.connect(energy_ball.set_ten)
	state.unit_changed.connect(energy_ball.set_unit)
	

func _physics_process(_delta: float) -> void:
	move_and_slide()


func _unhandled_input(event: InputEvent) -> void:
	if dying:
		return
	velocity = Vector2(
		Input.get_axis("left", "right"),
		0
	) * SPEED

	if event.is_action_pressed("shoot") and timer.time_left == 0:
		timer.start()
		var projectile =  Weapon.instantiate();
		
		shoot.emit(
			projectile,
			gun_ouput.global_position,
			state.ten,
			state.unit,
		)

	if event.is_action_pressed("change_ten_down") :
		state.ten -= 1
	if event.is_action_pressed("change_ten_up"):
		state.ten += 1
		
	if event.is_action_pressed("change_unit_down"):
		state.unit -= 1

	if event.is_action_pressed("change_unit_up"):

		state.unit += 1


func _on_area_2d_body_entered(body: Node2D) -> void:
	dying = true
	explosionSound.play()
	visible = false
	set_collision_layer_value(1, false)
	set_collision_layer_value(16, true)
	await explosionSound.finished
	died.emit()
	
	pass # Replace with function body.
