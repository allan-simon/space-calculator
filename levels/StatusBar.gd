extends Node

class_name StatusBar

@onready var score_label := $ScoreLabel
@onready var unit_bar : NumberBar = $UnitBar
@onready var ten_bar : NumberBar = $TenBar

var score := 0 : set = set_score
var player_state : PlayerState


func increase_score_by(to_add: int) -> void:
	score += to_add
	
func set_player_state(value: PlayerState) -> void:
	player_state = value
	player_state.unit_changed.connect(unit_bar.set_current_value)
	player_state.ten_changed.connect(ten_bar.set_current_value)
	
	player_state.ten = player_state.ten
	player_state.unit = player_state.unit

func set_score(new_score: int) -> void:
	score = max(0, new_score)
	score_label.text = "%s" % score

func reset() -> void :
	score = 0
