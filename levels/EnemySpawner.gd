extends Node

signal enemy_died
signal enemy_reached_objective

@export var enemyScene : PackedScene
@onready var spawnArea := $SpawnArea/CollisionShape2D
@onready var enemiesContainer := $EnemiesContainer

@export var min_number := 1
@export var max_number := 9

func spawn_enemy() -> void:

	var extents = spawnArea.shape.extents
	var origin = spawnArea.global_position
	print(extents)
	print(origin)
	
	var enemy : Enemy =  enemyScene.instantiate();
	enemy.enemy_died.connect(self.on_enemy_died)
	enemiesContainer.add_child(enemy)
	enemy.set_numbers(randi_range(min_number, max_number), randi_range(1, 9))
	enemy.global_position = Vector2(
		randi_range(origin.x - extents.x, origin.x + extents.x),
		randi_range(origin.y - extents.y, origin.y + extents.y),
	)

func on_enemy_died():
	enemy_died.emit()
	call_deferred("spawn_enemy")


func _on_enemy_objective_area_body_entered(body: Node2D) -> void:
	enemy_reached_objective.emit()
	body.queue_free()
	call_deferred("spawn_enemy")
	pass # Replace with function body.
