extends Node2D


@onready var player = $Player
@onready var projectile_manager = $ProjectileManager
@onready var backgroundParallax = $ParallaxBackground
@onready var backgroundLayer = $ParallaxBackground/ParallaxLayer
@onready var backgroundImage = $ParallaxBackground/ParallaxLayer/Background
@onready var enemy_spawner := $EnemySpawner
@onready var status_bar : StatusBar = $StatusBar
@onready var menu := $MenuLayer
@onready var first_button := $MenuLayer/Menu/VBoxContainer/ResumeButton

@export var min_number := 1
@export var max_number := 9


func _ready() -> void:
	VoskTextToSpeech.number_said.connect(_on_number_said)
	enemy_spawner.max_number = max_number
	enemy_spawner.min_number = min_number
	
	player.shoot.connect(projectile_manager.on_shoot)
	player.died.connect(self._on_player_died)
	player.state.ten_changed.connect(projectile_manager.on_ten_changed)
	player.state.unit_changed.connect(projectile_manager.on_unit_changed)
	player.state.ten = 4
	player.state.unit = 2
	status_bar.set_player_state(player.state)
	
	backgroundLayer.motion_mirroring.y = backgroundImage.get_rect().size.y
	enemy_spawner.spawn_enemy()
	enemy_spawner.spawn_enemy()
	enemy_spawner.spawn_enemy()
	
	get_tree().current_scene = self

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action("pause"):
		first_button.grab_focus()
		get_tree().paused = true
		menu.visible = true


func _physics_process(delta: float) -> void:
	backgroundParallax.scroll_offset.y += 100 * delta

func _on_player_died() -> void:
	print("dead")
	var level = load("res://levels/level_01.tscn").instantiate()
	level.min_number = self.min_number
	level.max_number = self.max_number

	get_tree().get_root().add_child(level)
	get_tree().get_root().remove_child(self)

func _on_enemy_died() -> void:
	status_bar.increase_score_by(1)


func _on_enemy_reached_objective() -> void:
	status_bar.increase_score_by(-1)


func _on_resume_button_pressed() -> void:
	get_tree().paused = false
	menu.visible = false



func _on_quit_button_pressed() -> void:
	get_tree().quit()


func _on_main_menu_button_pressed() -> void:
	get_tree().paused = false
	get_tree().change_scene_to_file("res://menus/main_menu.tscn")

func _on_number_said(ten, unit) -> void:
	player.state.ten = ten
	player.state.unit = unit


