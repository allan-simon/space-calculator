extends Node2D

class_name NumberBar

@onready var current_value_label := $CurrentValueLabel
@onready var previous_value_label := $PreviousValueLabel
@onready var next_value_label := $NextValueLabel

var current_value : int : set = set_current_value

func set_current_value(value :int) -> void:
	current_value = value
	current_value_label.text = "%s" % current_value
	if (current_value == 0):
		# without this, the previous value is -1 which is still -1
		# even after modulo 10
		previous_value_label.text = "9" 
	else:
		previous_value_label.text = "%s" % ((current_value - 1) % 10)
	
	next_value_label.text = "%s" % ((current_value + 1) % 10)
