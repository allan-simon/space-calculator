extends Control


@onready var table1Button := $VBoxContainer/Table1Button

func _ready() -> void:
	table1Button.grab_focus()

func _on_table_1_button_pressed() -> void:
	_load_level(1)
	pass # Replace with function body.


func _on_table_2_button_pressed() -> void:
	_load_level(2)
	pass # Replace with function body.


func _on_table_3_button_pressed() -> void:
	_load_level(3)
	pass # Replace with function body.


func _on_table_4_button_pressed() -> void:
	_load_level(4)
	pass # Replace with function body.


func _on_table_5_button_pressed() -> void:
	_load_level(5)
	pass # Replace with function body.


func _on_table_6_button_pressed() -> void:
	_load_level(6)
	pass # Replace with function body.


func _on_table_7_button_pressed() -> void:
	_load_level(7)
	pass # Replace with function body.


func _on_table_8_button_pressed() -> void:
	_load_level(8)
	pass # Replace with function body.


func _on_table_9_button_pressed() -> void:
	_load_level(9)
	pass # Replace with function body.


func _on_back_button_pressed() -> void:
	get_tree().change_scene_to_file("res://menus/main_menu.tscn")
	
func _load_level(table_of: int) -> void:
	var level = load("res://levels/level_01.tscn").instantiate()
	level.min_number = table_of
	level.max_number = table_of

	get_tree().get_root().add_child(level)
	get_tree().get_root().remove_child(self)
