extends CharacterBody2D

class_name  Enemy

signal enemy_died

@onready var first_number_label := $FirstNumber
@onready var second_number_label := $SecondNumber
@onready var operation_label := $OperationSign
@onready var explosionSound := $ExplosionSound
@onready var explosionAnimation := $Sprite2D/AnimationPlayer
@onready var missedSound := $MissedSound
@onready var exhaustAnimation := $ExhaustAnimation

var first_number : int = 2
var second_number : int = 7

var dying := false

const MIN_SPEED := 20
const SPEED := 40


func _ready() -> void:
	set_numbers(first_number, second_number)
	exhaustAnimation.play("exhaust")
	velocity.y +=  randi_range(MIN_SPEED, SPEED)

func _physics_process(delta: float) -> void:
	move_and_slide()


func receive_damage(total: int):
	if dying:
		return
	if total != first_number * second_number:
		missedSound.play(0.4)
		return
	enemy_died.emit()
	
	# enemy should not collide with the player
	# when exploding
	self.collision_layer = 0
	self.collision_mask = 0

	first_number_label.visible = false
	operation_label.visible = false
	second_number_label.visible = false
	dying = true
	self.remove_child(exhaustAnimation)
	explosionSound.play()
	explosionAnimation.play("pixel_explosion")
	await explosionSound.finished

	queue_free()



func set_numbers(first: int, second: int) ->void:

	first_number = first
	second_number = second
	first_number_label.text = "%s" % first
	second_number_label.text = "%s" % second
