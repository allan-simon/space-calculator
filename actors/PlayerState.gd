extends Node

class_name PlayerState

signal ten_changed(value: int)
signal unit_changed(value: int)

var ten := 0 : set = set_ten
var unit := 0 : set = set_unit


func set_ten(new_ten) -> void:
	if new_ten == -1:
		new_ten = 9
		
	
	ten = new_ten % 10
	ten_changed.emit(ten)

func set_unit(new_unit) -> void:
	
	if new_unit == -1:
		new_unit = 9
		# ten -= 1
	if new_unit == 10:
		# ten += 1
		new_unit = 0
	unit = new_unit
	
	unit_changed.emit(unit)
